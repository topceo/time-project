export function getUtcTime(timeObj, hoursDiff = 0) {
    if (!timeObj) {
        timeObj = new Date();
    }

    return Date.UTC(
        timeObj.getUTCFullYear(), timeObj.getUTCMonth(), timeObj.getUTCDate(),
        timeObj.getUTCHours() - hoursDiff, timeObj.getUTCMinutes(), timeObj.getUTCSeconds(),
    );
}

export function getDateFromUtc(timeInUTC, hoursDiff) {
    return new Date(timeInUTC + hoursDiff * 60 * 60 * 1000);
}