import React from 'react';
import './App.css';
import Header from './components/Header/Header';
import Footer from './components/Footer/Footer';
import TimeMonitor from "./components/TimeMonitor";

function App() {
    return (
        <div className="App">
            <Header/>
            <TimeMonitor/>
            <Footer/>
        </div>
    );
}

export default App;
