import React, {useState} from 'react';
import Clock from "./Clock";
import {getUtcTime, getDateFromUtc} from '../service/time.service';
import DateFnsUtils from '@date-io/date-fns';
import { MuiPickersUtilsProvider, KeyboardTimePicker } from '@material-ui/pickers';
import Box from '@material-ui/core/Box';
import { Button } from '@material-ui/core';

export default function TimeMonitor() {

    const [selectedDate, setSelectedDate] = useState(new Date());
    const utcTime = getUtcTime(selectedDate, 2); // from Kiev time

    const onChangeDate = date => {
        setSelectedDate(date)
    };

    const onResetTime = () => {
        setSelectedDate(new Date());
    };

    return (
        <div style={{paddingBottom: 100, minHeight: '100%'}}>
            <Box display="flex" justifyContent="center" flexWrap="wrap">
                <Clock date={getDateFromUtc(utcTime, 1)} city='London'/>
                <Clock date={getDateFromUtc(utcTime, 2)} city='Kiev'/>
                <Clock date={getDateFromUtc(utcTime, 3)} city='Dubai'/>
            </Box>
            <br/>

            <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <KeyboardTimePicker
                    margin="normal"
                    id="time-picker"
                    label="Time picker"
                    value={selectedDate}
                    onChange={onChangeDate}
                    KeyboardButtonProps={{
                        'aria-label': 'change time',
                    }}
                />
            </MuiPickersUtilsProvider>
            <br/>

            <Button variant="contained"
                    color="primary"
                    onClick={onResetTime}
            >
                Reset time
            </Button>
        </div>
    );
}