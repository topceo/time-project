import React from 'react';
import styles from './Header.module.css';
import Box from '@material-ui/core/Box';

export default function Header() {
    return (
        <header className={styles.mHeader}>
            <div className={styles.headWrapper}>
                <div className={styles.headTitle}>
                    <Box color="primary.main">TimeMonitor</Box>
                </div>
            </div>
        </header>
    )
}
