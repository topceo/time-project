import React from 'react';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
    card: {
        minWidth: 275,
        margin: 5,
    },
});

export default function Clock(props) {
    const classes = useStyles();
    const date = props.date;

    return (
        <Card className={classes.card}>
            <CardHeader title={props.city} />
            <CardContent>
                {
                    (date.getHours() < 10 ? `0${parseInt(date.getHours())}` : date.getHours())
                    + ':'
                    + (date.getMinutes() < 10 ? `0${date.getMinutes()}` : date.getMinutes())
                }
            </CardContent>
        </Card>

    );
}